//Import module router angular
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
//Component import exclusive
import { HomeComponent } from './components/home/home.component';
import { BlogComponent } from './components/blog/blog.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { PaginaComponent } from './components/pagina/pagina.component';
import { ErrorComponent } from './components/error/error.component';

//Array routes config
const appRoutes : Routes = [
    {path:'',component:HomeComponent},
    {path: 'home', component:HomeComponent},
    {path:'blog', component:BlogComponent},
    {path: 'formulario', component:FormularioComponent},
    {path: 'pagina-de-pruebas', component:PaginaComponent},
    {path: '**', component:ErrorComponent}


];
//export module routes
export const appRoutingProviders : any [] = [];
export const routing  : ModuleWithProviders =  RouterModule.forRoot(appRoutes);

//app.component.html

//<router-outlet></router-outlet>

//app.module.ts

//import { routing, appRoutingProviders} from './app.routing';

//Components home and others
//import { HomeComponent } from './components/home/home.component';

/*
imports: [
    BrowserModule,
    routing -> here
  ],

 providers: [appRoutingProviders],
 */

